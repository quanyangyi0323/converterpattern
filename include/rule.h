#ifndef CONVERTERPATTERN_RULE_H
#define CONVERTERPATTERN_RULE_H

#include <string>

namespace ConverterPattern
{
    template <typename SrcType, typename DestType>
    class Rule
    {
    public:
        Rule() = default;
        virtual ~Rule() = default;

        /// 指定された変換元データが条件を満たすか判定する
        virtual bool is_satisfied_by(const SrcType& number) const = 0;

        /// 指定された変換元データから変換した結果を返す
        virtual DestType convert(const SrcType& number) const = 0;
    };

    /// 3の倍数用のルール
    class RuleMultipleOf3 : public Rule<int, std::string>
    {
    public:
        RuleMultipleOf3() = default;
        ~RuleMultipleOf3() = default;

        bool is_satisfied_by(const int& number) const;
        std::string convert(const int& number) const;
    };

    /// 5の倍数用のルール
    class RuleMultipleOf5 : public Rule<int, std::string>
    {
    public:
        RuleMultipleOf5() = default;
        ~RuleMultipleOf5() = default;

        bool is_satisfied_by(const int& number) const;
        std::string convert(const int& number) const;
    };

    /// 15の倍数用のルール
    class RuleMultipleOf15 : public Rule<int, std::string>
    {
    public:
        RuleMultipleOf15() = default;
        ~RuleMultipleOf15() = default;

        bool is_satisfied_by(const int& number) const;
        std::string convert(const int& number) const;
    };

    /// 3の倍数でも5の倍数でもない場合用のルール
    class RuleElse : public Rule<int, std::string>
    {
    public:
        RuleElse() = default;
        ~RuleElse() = default;

        bool is_satisfied_by(const int& number) const;
        std::string convert(const int& number) const;
    };
}

#endif
