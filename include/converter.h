#ifndef CONVERTERPATTERN_CONVERTER_H
#define CONVERTERPATTERN_CONVERTER_H

#include <memory>
#include <vector>
#include "rule.h"

namespace ConverterPattern
{
    /**
     * @brief 変換ルールを動的に変更したい場合の変換器実装
     * @tparam SrcType 変換元データの型
     * @tparam DestType 変換先データの型
     */
    template <typename SrcType, typename DestType>
    class Converter
    {
    public:
        using rule_type = std::unique_ptr<Rule<SrcType, DestType>>;

    private:
        std::vector<rule_type> m_rules;
        rule_type m_default_rule;

    public:
        Converter() = default;
        ~Converter() = default;

        void add(rule_type&& rule)
        {
            m_rules.push_back(std::move(rule));
        }

        void add_as_default(rule_type&& rule)
        {
            m_default_rule = std::move(rule);
        }

        DestType convert(const SrcType& src) const
        {
            // 登録されているルール群から、引数srcに対応したルールを探す
            auto result = std::find_if(
                m_rules.begin(),
                m_rules.end(),
                [&src](const auto& rule) { return rule->is_satisfied_by(src); });

            if (result != m_rules.end())
            {
                return (*result)->convert(src);
            }

            // 対応したルールが登録されていなかったのでデフォルトルールを用いる
            return m_default_rule->convert(src);
        }
    };

    class SimpleConverter
    {
    public:
        SimpleConverter() = default;
        ~SimpleConverter() = default;

        std::string convert(int number) const;
    };
}

#endif
