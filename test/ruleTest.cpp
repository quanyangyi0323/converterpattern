#include <catch.hpp>
#include "rule.h"
using namespace ConverterPattern;

TEST_CASE("RuleMultipleOf3::is_satisfied_by", "[RuleMultipleOf3]")
{
    RuleMultipleOf3 sut;
    REQUIRE(!sut.is_satisfied_by(1));
    REQUIRE(sut.is_satisfied_by(3));
    REQUIRE(!sut.is_satisfied_by(5));
}

TEST_CASE("RuleMultipleOf5::is_satisfied_by", "[RuleMultipleOf5]")
{
    RuleMultipleOf5 sut;
    REQUIRE(!sut.is_satisfied_by(1));
    REQUIRE(!sut.is_satisfied_by(3));
    REQUIRE(sut.is_satisfied_by(5));
}

TEST_CASE("RuleElse::is_satisfied_by", "[RuleElse]")
{
    RuleElse sut;
    REQUIRE(sut.is_satisfied_by(1));
    REQUIRE(!sut.is_satisfied_by(3));
    REQUIRE(!sut.is_satisfied_by(5));
}
