#include <catch.hpp>
#include "converter.h"
#include "rule.h"
using namespace ConverterPattern;

TEST_CASE("Converter::convert", "[Converter]")
{
    Converter<int, std::string> sut;
    sut.add(std::make_unique<RuleMultipleOf15>());
    sut.add(std::make_unique<RuleMultipleOf3>());
    sut.add(std::make_unique<RuleMultipleOf5>());
    sut.add_as_default(std::make_unique<RuleElse>());

    REQUIRE(sut.convert(1) == "1");
    REQUIRE(sut.convert(3) == "fizz");
    REQUIRE(sut.convert(5) == "buzz");
    REQUIRE(sut.convert(15) == "fizzbuzz");
}

#include <chrono>

namespace
{
    const int LOOP_COUNT = 100000;
}

TEST_CASE("Converter::convert_性能測定", "[Converter]")
{
    Converter<int, std::string> sut;
    sut.add(std::make_unique<RuleMultipleOf15>());
    sut.add(std::make_unique<RuleMultipleOf3>());
    sut.add(std::make_unique<RuleMultipleOf5>());
    sut.add_as_default(std::make_unique<RuleElse>());

    int counter_fizz = 0;
    int counter_buzz = 0;
    int counter_fizzbuzz = 0;

    for (int i = 0; i < LOOP_COUNT; ++i)
    {
        auto result = sut.convert(i);

        if (result == "fizz")
        {
            ++counter_fizz;
        }
        else if (result == "buzz")
        {
            ++counter_buzz;
        }
        else if (result == "fizzbuzz")
        {
            ++counter_fizzbuzz;
        }
    }

    REQUIRE(counter_fizz == 26667);
    REQUIRE(counter_buzz == 13333);
    REQUIRE(counter_fizzbuzz == 6667);
}

TEST_CASE("SimpleConverter::convert_性能測定", "[SimpleConverter]")
{
    SimpleConverter sut;

    int counter_fizz = 0;
    int counter_buzz = 0;
    int counter_fizzbuzz = 0;

    for (int i = 0; i < LOOP_COUNT; ++i)
    {
        auto result = sut.convert(i);

        if (result == "fizz")
        {
            ++counter_fizz;
        }
        else if (result == "buzz")
        {
            ++counter_buzz;
        }
        else if (result == "fizzbuzz")
        {
            ++counter_fizzbuzz;
        }
    }

    REQUIRE(counter_fizz == 26667);
    REQUIRE(counter_buzz == 13333);
    REQUIRE(counter_fizzbuzz == 6667);
}
