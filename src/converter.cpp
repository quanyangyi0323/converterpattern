#include "converter.h"
#include <algorithm>
using namespace ConverterPattern;

std::string SimpleConverter::convert(int number) const
{
    if ((number % 15) == 0)
    {
        return "fizzbuzz";
    }

    if ((number % 3) == 0)
    {
        return "fizz";
    }

    if ((number % 5) == 0)
    {
        return "buzz";
    }

    return std::to_string(number);
}
