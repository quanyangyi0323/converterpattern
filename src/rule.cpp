#include "rule.h"
using namespace ConverterPattern;

bool RuleMultipleOf3::is_satisfied_by(const int& number) const
{
    return ((number % 3) == 0);
}

std::string RuleMultipleOf3::convert(const int& number) const
{
    return "fizz";
}

bool RuleMultipleOf5::is_satisfied_by(const int& number) const
{
    return ((number % 5) == 0);
}

std::string RuleMultipleOf5::convert(const int& number) const
{
    return "buzz";
}

bool RuleMultipleOf15::is_satisfied_by(const int& number) const
{
    return ((number % 15) == 0);
}

std::string RuleMultipleOf15::convert(const int& number) const
{
    return "fizzbuzz";
}

bool RuleElse::is_satisfied_by(const int& number) const
{
    return ((number % 3) != 0) && ((number % 5) != 0);
}

std::string RuleElse::convert(const int& number) const
{
    return std::to_string(number);
}
